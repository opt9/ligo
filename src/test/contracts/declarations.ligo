// Test PascaLIGO top level declarations

const foo : int = 42

function main (const i : int) : int is
  begin
    skip
  end with i + foo
