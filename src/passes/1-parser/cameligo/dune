(ocamllex LexToken)

(menhir
  (merge_into Parser)
  (modules ParToken Parser)
  (flags -la 1 --table --strict --explain --external-tokens LexToken))

(library
  (name parser_cameligo)
  (public_name ligo.parser.cameligo)
  (modules AST cameligo Parser ParserLog LexToken)
  (libraries
     menhirLib
     parser_shared
     str
     simple-utils
     tezos-utils
     getopt)
  (preprocess
    (pps bisect_ppx --conditional))
  (flags (:standard -open Simple_utils -open Parser_shared)))

(executable
  (name LexerMain)
  (libraries parser_cameligo)
  (modules LexerMain)
  (preprocess
    (pps bisect_ppx --conditional))
  (flags (:standard -open Parser_shared -open Parser_cameligo)))

(executable
  (name ParserMain)
  (libraries parser_cameligo)
  (modules
     ParErr ParserMain)
  (preprocess
    (pps bisect_ppx --conditional))
  (flags (:standard -open Simple_utils -open Parser_shared -open Parser_cameligo)))

(executable
  (name Unlexer)
  (libraries str)
  (preprocess
    (pps bisect_ppx --conditional))
  (modules Unlexer))

(rule
  (targets Parser.msg)
  (deps (:script_messages ../../../../vendors/ligo-utils/simple-utils/messages.sh) Parser.mly LexToken.mli ParToken.mly)
  (action (run %{script_messages} --lex-tokens=LexToken.mli --par-tokens=ParToken.mly Parser.mly )))

(rule
  (targets all.ligo)
  (deps (:script_cover ../../../../vendors/ligo-utils/simple-utils/cover.sh) Parser.mly LexToken.mli ParToken.mly Parser.msg Unlexer.exe)
  (action (run %{script_cover} --lex-tokens=LexToken.mli --par-tokens=ParToken.mly --ext=ligo --unlexer=./Unlexer.exe --messages=Parser.msg --dir=. --concatenate Parser.mly )))